from flask import Flask, request, jsonify
from flask_restful import Resource, Api
from json import dumps
from pymysql import connect, cursors


hostname = '52.77.135.122'
username = 'newuser2'
password = 'password'
database = 'sister_tugas'
'''

hostname = '192.168.0.90'
username = 'root'
password = 'newpassword'
database = 'sister_tugas'
'''

def Connect():
    con = connect(host=hostname, 
                  user=username, 
                  passwd=password, 
                  db=database,
                  charset='utf8mb4',
                  cursorclass=cursors.DictCursor)
    return con

app = Flask(__name__)

default_response = {
    "success" : "true",
    "data" : ""
}

@app.route('/get/pasien', methods=['GET'])
def getPasien():
    response = default_response.copy()
    try:
        conn = Connect()    
        with conn.cursor() as cursor:
            sql = "SELECT id_pasien, ktp, nama, jenisKelamin, tanggalLahir, golDarah, noTelp, alamat FROM pasien ORDER BY id_pasien"
            cursor.execute(sql)
            result = cursor.fetchall()
        response["data"] = result
        conn.close()
    except:
        response["success"] = "false"
    return jsonify(response)

@app.route('/get/dokter', methods=['GET'])
def getDokter():
    response = default_response.copy()
    try:
        conn = Connect()
        with conn.cursor() as cursor:
            sql = "SELECT id_dokter, nama, spesialis, no_handphone FROM dokter ORDER BY id_dokter"
            cursor.execute(sql)
            result = cursor.fetchall()
        response["data"] = result
        conn.close()
    except:
        response["success"] = "false"
    return jsonify(response)

@app.route('/get/riwayat', methods=['GET'])
def getAllRiwayat():
    response = default_response.copy()
    try:
        conn = Connect()
        with conn.cursor() as cursor:
            sql = '''
            SELECT riwayat.id_transaksi, riwayat.tanggal, riwayat.keluhan, riwayat.diagnosa, riwayat.checked, 
            dokter.id_dokter, dokter.nama as namaDokter, dokter.spesialis, dokter.no_handphone, 
            pasien.id_pasien, pasien.ktp, pasien.nama as namaPasien, pasien.jenisKelamin, pasien.tanggalLahir, 
            pasien.golDarah, pasien.noTelp, pasien.alamat 
            FROM riwayat, pasien, dokter 
            WHERE riwayat.id_pasien = pasien.id_pasien AND riwayat.id_dokter = dokter.id_dokter AND riwayat.checked = 0
			ORDER BY riwayat.id_transaksi
            '''
            cursor.execute(sql)
            result = cursor.fetchall()
        response["data"] = result
        conn.close()
    except:
        response["success"] = "false"
    return jsonify(response)

@app.route('/get/riwayat/dokter/<int:idDokter>', methods=['GET'])
def getRiwayatByDokter(idDokter):
    response = default_response.copy()
    try:
    	conn = Connect()
        with conn.cursor() as cursor:
            sql = '''
            SELECT riwayat.id_transaksi, riwayat.tanggal, riwayat.keluhan, riwayat.diagnosa, riwayat.checked, 
            dokter.id_dokter, dokter.nama as namaDokter, dokter.spesialis, dokter.no_handphone, 
            pasien.id_pasien, pasien.ktp, pasien.nama as namaPasien, pasien.jenisKelamin, pasien.tanggalLahir, 
            pasien.golDarah, pasien.noTelp, pasien.alamat 
            FROM riwayat, pasien, dokter 
            WHERE riwayat.id_pasien = pasien.id_pasien AND 
            riwayat.id_dokter = dokter.id_dokter AND 
            riwayat.id_dokter = {} AND
            riwayat.checked = 0
			ORDER BY riwayat.id_transaksi
            '''.format(idDokter)
            cursor.execute(sql)
            result = cursor.fetchall()
        response["data"] = result
        conn.close()
    except:
        response["success"] = "false"
    return jsonify(response)

@app.route('/get/riwayat/<int:idPasien>', methods=['GET'])
def getRiwayatById(idPasien):
    response = default_response.copy()
    try:
        conn = Connect()
        with conn.cursor() as cursor:
            sql = '''
            SELECT riwayat.id_transaksi, riwayat.tanggal, riwayat.keluhan, riwayat.diagnosa, riwayat.checked, 
            dokter.id_dokter, dokter.nama as namaDokter, dokter.spesialis, dokter.no_handphone, 
            pasien.id_pasien, pasien.ktp, pasien.nama as namaPasien, pasien.jenisKelamin, pasien.tanggalLahir, 
            pasien.golDarah, pasien.noTelp, pasien.alamat 
            FROM riwayat, pasien, dokter 
            WHERE riwayat.id_pasien = pasien.id_pasien AND 
                riwayat.id_dokter = dokter.id_dokter AND
                riwayat.id_pasien = {}
			ORDER BY riwayat.id_transaksi
            '''.format(idPasien)
            cursor.execute(sql)
            result = cursor.fetchall()
        response["data"] = result
        conn.close()
    except:
        response["success"] = "false"
    return jsonify(response)

@app.route('/edit/pasien', methods=['POST'])
def editPasien():
    content = request.get_json(silent=True)
    id_pasien = content['id_pasien']
    ktp = content['ktp']
    nama = content['nama']
    jenisKelamin = content['jenisKelamin']
    tanggalLahir = content['tanggalLahir']
    golDarah = content['golDarah']
    noTelp = content['noTelp']
    alamat = content['alamat']

    response = default_response.copy()
    try:
        conn = Connect()    
        with conn.cursor() as cursor:
            sql = '''
            UPDATE pasien 
            SET
                ktp='{}',
                nama='{}',
                jenisKelamin='{}',
                tanggalLahir='{}',
                golDarah='{}',
                noTelp='{}',
                alamat='{}'
            WHERE id_pasien = {}
            '''.format(ktp, nama, jenisKelamin, tanggalLahir, golDarah, noTelp, alamat, id_pasien)
            print sql
            cursor.execute(sql)
        conn.commit()
        conn.close()
    except:
        response["success"] = "false"
    return jsonify(response)

@app.route('/add/pasien', methods=['POST'])
def addPasien():
    content = request.get_json(silent=True)
    ktp = content['ktp']
    nama = content['nama']
    jenisKelamin = content['jenisKelamin']
    tanggalLahir = content['tanggalLahir']
    golDarah = content['golDarah']
    noTelp = content['noTelp']
    alamat = content['alamat']

    response = default_response.copy()
    try:
        conn = Connect()    
        with conn.cursor() as cursor:
            sql = '''
                INSERT INTO `pasien` (`ktp`, `nama`, `jenisKelamin`, `tanggalLahir`, `golDarah`, `noTelp`, `alamat`) 
                VALUES (
                    '{}',
                    '{}',
                    '{}',
                    '{}',
                    '{}',
                    '{}',
                    '{}'
                )'''.format(ktp, nama, jenisKelamin, tanggalLahir, golDarah, noTelp, alamat)
            print sql
            cursor.execute(sql)
        conn.commit()
        conn.close()
    except:
        response['success'] = 'false'
    return jsonify(response)

@app.route('/add/riwayat', methods=['POST'])
def addRiwayat():
    content = request.get_json(silent=True)
    tanggal = content['tanggal']
    id_pasien = content['id_pasien']
    id_dokter = content['id_dokter']
    keluhan = content['keluhan']

    response = default_response.copy()
    try:
        conn = Connect()    
        with conn.cursor() as cursor:
            sql = '''
                INSERT INTO `riwayat` (`tanggal`, `id_pasien`, `id_dokter`, `keluhan`, `diagnosa`, `checked`) 
                    VALUES ('{}','{}','{}','{}','','0')
                '''.format(tanggal, id_pasien, id_dokter, keluhan)
            print sql
            cursor.execute(sql)
        conn.commit()
        conn.close()
    except:
        response['success'] = 'false'
    return jsonify(response)

@app.route('/edit/riwayat', methods=['POST'])
def editRiwayat():
    content = request.get_json(silent=True)
    id_transaksi = content['id_transaksi']
    tanggal = content['tanggal']
    id_pasien = content['id_pasien']['id_pasien']
    id_dokter = content['id_dokter']['id_dokter']
    keluhan = content['keluhan']
    diagnosa = content['diagnosa']

    response = default_response.copy()
    try:
        conn = Connect()    
        with conn.cursor() as cursor:
            sql = '''
            UPDATE riwayat
            SET
                tanggal='{}',
                id_pasien='{}',
                id_dokter='{}',
                keluhan='{}',
                diagnosa='{}',
				checked=1
            WHERE id_transaksi = {}
            '''.format(tanggal, id_pasien, id_dokter, keluhan, diagnosa, id_transaksi)
            print sql
            cursor.execute(sql)
        conn.commit()
        conn.close()
    except:
        response['success'] = 'false'
    return jsonify(response)

if __name__ == '__main__':
     app.run(debug=True, host='0.0.0.0')