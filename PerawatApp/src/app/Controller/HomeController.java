package app.Controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Modality;
import javafx.stage.Stage;
import app.Data.DataContainer.Pasien;


public class HomeController extends BaseController {
    @FXML
    public void initialize(){

    }


    @FXML protected Button btnDaftarPasien, btnDataPasien;

    @FXML
    protected ObservableList<Pasien> pasiens =
            FXCollections.observableArrayList(
                    new Pasien(1, "123123","Charles Anderson","P","01/01/1996","O","08561235123","Newton"),
                    new Pasien(2, "456456","Yudha Teguh H","W","01/05/1996","O","0856132565758","Valak")
            );

    @FXML protected void handleBtnPendaftaran(){
        try {
            FXMLLoader roomListLoader = newForm("PendaftaranPasien.fxml", "Daftar List", 600, 900, true);
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }

    @FXML protected void handleBtnDaftar(){
        try {
            FXMLLoader roomListLoader = newForm("DaftarPasien.fxml", "Daftar List", 600, 900, false);
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }
}
