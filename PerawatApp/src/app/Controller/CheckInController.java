package app.Controller;

import app.Data.DAL.DokterDAL;
import app.Data.DAL.PasienDAL;
import app.Data.DataContainer.Pasien;
import app.Data.DataContainer.Riwayat;
import com.google.gson.Gson;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class CheckInController extends BaseController{

    @FXML
    protected Hyperlink hyp_riwayat;
    @FXML protected TextArea txtAlamat, txtKeluhan;
    @FXML protected TextField txtNama, txtTanggalLahir, txtJenisKelamin, txtGolDarah, txtNoTelp;
    @FXML
    protected ComboBox cbDokter;
    @FXML
    protected Button btnCheckIn;

    @FXML protected ComboBox<String> cbJenisKelamin;
    Pasien dataPasien = null;

    public void loadData(Pasien p){
        this.dataPasien = p;
        txtNama.setText(p.getNama());
        txtTanggalLahir.setText(p.getTanggalLahir());
        txtGolDarah.setText(p.getGolDarah());
        System.out.println(dataPasien.getNama());

        new Thread(new Runnable() {
            @Override
            public void run() {
                DokterDAL dokterDAL = new DokterDAL();
                ObservableList<String> listDokter = null;
                try {
                    listDokter = dokterDAL.loadDokter();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                cbDokter.setItems(listDokter);
            }
        }).start();

    }
    @FXML
    public void handleBtnCheckIn(){

        if(!txtKeluhan.getText().equals("") && cbDokter.getSelectionModel().getSelectedItem() != null)
        {

            Riwayat riwayat = new Riwayat();
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            String strDate = sdf.format(cal.getTime());
            System.out.println("Current date in String Format: " + strDate);

            riwayat.setTanggal(strDate);
            riwayat.setId_pasien(dataPasien.getId());
            riwayat.setId_dokter(cbDokter.getSelectionModel().getSelectedIndex()+1);
            riwayat.setKeluhan(txtKeluhan.getText());

            try {
                String json = new Gson().toJson(riwayat);
                System.out.println(json);

                PasienDAL pasienDAL = new PasienDAL();
                if (pasienDAL.checkIn(json)) {
                    showAlert(Alert.AlertType.INFORMATION, "Information", null, "Check In Success");
                    Stage stage = (Stage) btnCheckIn.getScene().getWindow();
                    stage.close();
                } else {
                    showAlert(Alert.AlertType.INFORMATION, "Information", null, "Check In Failed");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            showAlert(Alert.AlertType.INFORMATION, "Information", null, "Please fill all data");
        }
    }

}
