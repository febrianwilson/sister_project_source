package app.Controller;

import app.Data.ConnectionConfiguration;
import app.Data.DAL.BaseDal;
import app.Data.DAL.PasienDAL;
import app.Data.DataContainer.Pasien;
import com.jfoenix.controls.JFXTextField;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Duration;

import javax.xml.soap.Detail;
import java.io.IOException;
import java.util.ArrayList;

public class DaftarPasienController extends BaseController{

    @FXML protected Button btn_CheckIn, btn_Detail;
    @FXML protected TableView<Pasien> table_pasien;
    @FXML protected TableColumn col_Id, col_Nama, col_TanggalLahir, col_Ktp;
    @FXML protected TextField txtSearch;

    protected ObservableList<Pasien> pasiens = FXCollections.emptyObservableList();
    protected ObservableList<Pasien> filteredPasien = null;



    private Timeline refreshRiwayat = null;

    public void initialize(){
        col_Id.setCellValueFactory(new PropertyValueFactory<Pasien,Integer>("id"));
        col_Ktp.setCellValueFactory(new PropertyValueFactory<Pasien,String>("ktp"));
        col_Nama.setCellValueFactory(new PropertyValueFactory<Pasien,String>("nama"));
        col_TanggalLahir.setCellValueFactory(new PropertyValueFactory<Pasien,String>("tanggalLahir"));

        new Thread(() -> {
            PasienDAL pasienDAL = new PasienDAL();
            try {
                pasiens = pasienDAL.loadPasien();
            } catch (IOException e) {
                //showFail();
            }
            String keyword = "";
            filteredPasien = pasiens.filtered(pasien -> pasien.getNama().toLowerCase().contains(keyword));
            table_pasien.setItems(filteredPasien);
        }).start();
        Timeline getPasienRefresh = new Timeline(new KeyFrame(Duration.seconds(10), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    PasienDAL pasienDAL = new PasienDAL();
                    if (connectionStatus) {
                        try {
                            pasiens = pasienDAL.loadPasien();
                        } catch (Exception e) {
                            if(e.getMessage().equals("java.io.IOException: falses")){
                                showFail1();
                            }else {
                                showFail();
                            }
                            connectionStatus = false;
                            System.out.println("HERE THROW"+ e.getMessage());
                            e.printStackTrace();
                        }
                        String keyword = txtSearch.getText().toLowerCase();
                        filteredPasien = pasiens.filtered(pasien -> pasien.getNama().toLowerCase().contains(keyword));
                        table_pasien.setItems(filteredPasien);
                    } else {
                        try {
                            pasiens = null;
                            pasiens = pasienDAL.loadPasien();
                            if (!pasiens.isEmpty()) {
                                String keyword = txtSearch.getText().toLowerCase();
                                filteredPasien = pasiens.filtered(pasien -> pasien.getNama().toLowerCase().contains(keyword));
                                table_pasien.setItems(filteredPasien);
                                connectionStatus = true;
                                showOK();
                            }
                        } catch (Exception e) {
                            connectionStatus = false;
                            e.printStackTrace();
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }));

        getPasienRefresh.setCycleCount(Animation.INDEFINITE);
        getPasienRefresh.play();

        txtSearch.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredPasien = pasiens.filtered(pasien -> pasien.getNama().toLowerCase().contains(newValue.toLowerCase()));
            table_pasien.setItems(filteredPasien);
        });
    }


    @FXML protected void handleBtnDetail(){
        Pasien p = filteredPasien.get(table_pasien.getSelectionModel().getSelectedIndex());
        System.out.println("from Handle"+ p.getNama());
        try {
            FXMLLoader roomListLoader = newForm("DetailPasien.fxml", "Daftar List", 600, 900, false);
            DetailPasienController detailPasienController = roomListLoader.getController();
            detailPasienController.loadPasien(p);
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }
    @FXML protected void handleBtnCheckIn(){
        Pasien p = filteredPasien.get(table_pasien.getSelectionModel().getSelectedIndex());
        System.out.println("from Handle"+ p.getNama());
        try {
            FXMLLoader roomListLoader = newForm("CheckIn.fxml", "Daftar List", 600, 900, false);
            CheckInController checkInController = roomListLoader.getController();
            checkInController.loadData(p);
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }



}
