package app.Controller;

import app.Controller.BaseController;
import app.Data.ConnectionConfiguration;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class SetupController extends BaseController {
    @FXML TextField txtIP;

    @FXML
    public void handleBtnConnect() {
        String ip = txtIP.getText().toString();
        ConnectionConfiguration.setServer(ip);
        if (ConnectionConfiguration.checkConnection()) {
            showAlert(Alert.AlertType.INFORMATION, "Information", null, "Connected");
            Stage stage = (Stage) txtIP.getScene().getWindow();
            stage.hide();
            newForm("Home.fxml", "Daftar Pasien", 600, 500, true);
            stage.show();
        } else {
            showAlert(Alert.AlertType.INFORMATION, "Information", null, "Failed to connect");
        }
    }
}
