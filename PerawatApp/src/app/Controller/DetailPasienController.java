package app.Controller;

import app.Data.ConnectionConfiguration;
import app.Data.DataContainer.Pasien;
import app.Data.DAL.PasienDAL;
import app.Data.DataContainer.Pasien;
import com.google.gson.Gson;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.stage.Stage;
import javafx.util.Duration;

public class DetailPasienController extends BaseController{
    @FXML
    protected Hyperlink hyp_riwayat;
    Pasien dataPasien = null;
    @FXML protected TextArea txtAlamat;
    @FXML protected TextField txtNama, txtKTP, txtTanggalLahir, txtJenisKelamin, txtGolDarah, txtNoTelp;
    @FXML
    protected Button btnUpdate;

    @FXML protected ComboBox<String> cbJenisKelamin;


    protected void loadPasien(Pasien p){
        this.dataPasien = p;
        System.out.println("ID : "+ dataPasien.getId().toString());
        txtNama.setText(this.dataPasien.getNama());
        txtAlamat.setText(this.dataPasien.getAlamat());
        txtGolDarah.setText(this.dataPasien.getGolDarah());
        txtKTP.setText(this.dataPasien.getKtp());
        txtNoTelp.setText(this.dataPasien.getNoTelp());
        txtTanggalLahir.setText(this.dataPasien.getTanggalLahir());
        if(this.dataPasien.getJenisKelamin().equals("P")) {
            cbJenisKelamin.getSelectionModel().select(1);
        }else{
            cbJenisKelamin.getSelectionModel().select(0);
        }
    }
    @FXML
    protected void handleBtnRiwayat(){
        try {
            FXMLLoader roomListLoader = newForm("RiwayatPasien.fxml", "Riwayat List", 600, 900, false);
            RiwayatPasienController riwayatPasienController = roomListLoader.getController();
            riwayatPasienController.loadId(dataPasien);
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }
    @FXML
    protected void handleBtnUpdate(){


        if(!txtNama.getText().equals("") &&
                !txtKTP.getText().equals("")&&
                !txtGolDarah.getText().equals("")&&
                !txtTanggalLahir.getText().equals("")&&
                !txtNoTelp.getText().equals("")&&
                !txtAlamat.getText().equals("")&&
                !cbJenisKelamin.getSelectionModel().getSelectedItem().equals("")
                )
        {
            Pasien p = new Pasien();
            p.setId(dataPasien.getId());

            System.out.println("ID 2: "+ p.getId().toString());
            p.setNama(txtNama.getText());
            p.setKtp(txtKTP.getText());
            p.setGolDarah(txtGolDarah.getText());
            p.setTanggalLahir(txtTanggalLahir.getText());
            p.setNoTelp(txtNoTelp.getText());
            p.setAlamat(txtAlamat.getText());
            p.setJenisKelamin(cbJenisKelamin.getSelectionModel().getSelectedItem());

            try {
                String json = new Gson().toJson(p);
                System.out.println(json);

                PasienDAL pasienDAL = new PasienDAL();
                if (pasienDAL.editPasien(json)) {
                    showAlert(Alert.AlertType.INFORMATION, "Information", null, "Update Pasien Success");
                    Stage stage = (Stage) btnUpdate.getScene().getWindow();
                    stage.close();
                } else {
                    showAlert(Alert.AlertType.INFORMATION, "Information", null, "Update Pasien Failed");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            showAlert(Alert.AlertType.INFORMATION, "Information", null, "Please fill all data");
        }

    }


}

