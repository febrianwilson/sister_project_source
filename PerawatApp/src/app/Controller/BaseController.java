package app.Controller;

import app.Data.ConnectionConfiguration;
import com.sun.deploy.config.Config;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.stage.Stage;
import javafx.util.Duration;
import sun.plugin2.message.Message;

import java.sql.Connection;
import java.util.Date;
import java.util.Properties;

public class BaseController {

    public Timeline connectionChecker = null;
    public Boolean connectionStatus = true;

    protected FXMLLoader newForm(String resource, String title, double height, double width, boolean showAndWait){
        resource = "../UI/" + resource;
        System.out.println(resource);

        FXMLLoader loader = null;
        try{

            loader = new FXMLLoader(getClass().getResource(resource));
            Parent formRegister = loader.load();
            Stage stageRegister = new Stage();
            stageRegister.setTitle(title);
            stageRegister.setScene(new Scene(formRegister, width, height));

            if(showAndWait) stageRegister.showAndWait();
            else stageRegister.show();

            stageRegister.requestFocus();
        }catch (Exception ex){
            ex.printStackTrace();
        }

        return loader;
    }

    protected void showDialog(String title, Exception ex){
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(ex.getMessage());
        alert.showAndWait();
    }

    //Usage example: showAlert(Alert.AlertType.WARNING, "Warning Information", null, "Username / Password Incorrect");
    public void showAlert(Alert.AlertType alertType, String title, String header, String content) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(content);
        alert.showAndWait();
    }
    public void showFail() {
        showAlert(Alert.AlertType.WARNING, "Information", null, "Server is offline\nTrying to connect on background...", false);
    }
    public void showFail1() {
        showAlert(Alert.AlertType.WARNING, "Information", null, "Server is online\nbut unable to fetch data, please wait until database up\n", false);
    }
    public void showOK() {
        showAlert(Alert.AlertType.WARNING, "Information", null, "Server is Online", false);
    }




    public static void showAlert(Alert.AlertType alertType, String title, String header, String content, boolean isWait) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(content);
        alert.show();
    }



}

