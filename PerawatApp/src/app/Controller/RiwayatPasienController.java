package app.Controller;

import app.Data.DAL.PasienDAL;
import app.Data.DataContainer.Pasien;
import app.Data.DataContainer.Riwayat;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.io.IOException;

public class RiwayatPasienController {
    protected Pasien dataPasien;
    @FXML
    protected ObservableList<Riwayat> listRiwayat;


    @FXML protected TableView<Riwayat> table_riwayat;
    @FXML protected Label lblNama;
    @FXML protected TableColumn col_Id, col_Tanggal, col_Dokter, col_Keluhan;

    protected void loadId(Pasien p){
        this.dataPasien = p;
        PasienDAL pasienDAL = new PasienDAL();
        try {
            listRiwayat = pasienDAL.loadRiwayat(p.getId());
        } catch (IOException e) {
            table_riwayat.setAccessibleText("FAIL");
            e.printStackTrace();
        }
        table_riwayat.setItems(listRiwayat);
        lblNama.setText("NAMA : "+p.getNama());

    }


    public void initialize()
    {
        col_Id.setCellValueFactory(new PropertyValueFactory<Riwayat,Integer>("id_pasien"));
        col_Tanggal.setCellValueFactory(new PropertyValueFactory<Riwayat,String>("tanggal"));
        col_Keluhan.setCellValueFactory(new PropertyValueFactory<Riwayat,String>("keluhan"));
        col_Dokter.setCellValueFactory(new PropertyValueFactory<Riwayat,String>("namaDokter"));
    }


}
