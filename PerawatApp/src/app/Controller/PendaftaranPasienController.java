package app.Controller;

import app.Data.DAL.DokterDAL;
import app.Data.DAL.PasienDAL;
import app.Data.DataContainer.Dokter;
import app.Data.DataContainer.Pasien;
import com.google.gson.Gson;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.util.Random;

public class PendaftaranPasienController extends BaseController {

    @FXML
    protected Button btnDaftar;
    @FXML protected TextField txtNama, txtKTP, txtTanggalLahir, txtNoTelp, txtGolDarah;
    @FXML protected TextArea txtAlamat, txtKeluhan;
    @FXML protected ComboBox<String> cbJenisKelamin;

    public void initialize(){
    }

    @FXML public void handleBtnDaftar()
    {
        if(!txtNama.getText().equals("") &&
                !txtKTP.getText().equals("")&&
                !txtGolDarah.getText().equals("")&&
                !txtTanggalLahir.getText().equals("")&&
                !txtNoTelp.getText().equals("")&&
                !txtAlamat.getText().equals("")&&
                !cbJenisKelamin.getSelectionModel().getSelectedItem().equals("")
                )
        {
            Pasien p = new Pasien();
            p.setNama(txtNama.getText());
            p.setKtp(txtKTP.getText());
            p.setGolDarah(txtGolDarah.getText());
            p.setTanggalLahir(txtTanggalLahir.getText());
            p.setNoTelp(txtNoTelp.getText());
            p.setAlamat(txtAlamat.getText());
            p.setJenisKelamin(cbJenisKelamin.getSelectionModel().getSelectedItem());

            try {
                String json = new Gson().toJson(p);
                System.out.println(json);

                PasienDAL pasienDAL = new PasienDAL();
                if (pasienDAL.pendaftaranPasien(json)) {
                    showAlert(Alert.AlertType.INFORMATION, "Information", null, "Add Pasien Success");
                    Stage stage = (Stage) btnDaftar.getScene().getWindow();
                    stage.close();
                } else {
                    showAlert(Alert.AlertType.INFORMATION, "Information", null, "Add Pasien Failed");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            showAlert(Alert.AlertType.INFORMATION, "Information", null, "Please fill all data");
        }

    }


}
