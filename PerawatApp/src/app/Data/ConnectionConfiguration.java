package app.Data;

import java.io.IOException;
import java.net.Socket;
import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectionConfiguration {
    public static String serverLocation = "127.0.0.1";
    public static Integer port = 5000;
    private static final String username = "root";
    private static final String password = "";

    /*private static final String connectionString = "jdbc:mysql://localhost:3306/dbhotel";
    private static final String username = "root";
    private static final String password = "*";*/

    public static void setServer(String ip){
        if(ip.contains(":")){
            String[] s = ip.split(":");
            serverLocation = s[0];
            port = Integer.parseInt(s[1]);
        }else{
            serverLocation = ip;
        }
    }

    public static boolean checkConnection(){
        try (Socket s = new Socket(serverLocation, port)) {
            return true;
        } catch (IOException ex) {
        /* ignore */
        }
        return false;
    }
}
