package app.Data.DAL;

import app.Data.ConnectionConfiguration;
import com.google.gson.Gson;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;


public class BaseDal {

    protected static boolean connect(){
        boolean b = ConnectionConfiguration.checkConnection();
        if(b){
            System.out.println("Connection Estabilished!");
            return true;
        }
        return  false;
    }
    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

    public static InputStream OpenHttpConnection(String urlString, String method, String params) throws IOException {
        InputStream inputStream = null;
        String address ="http://"+ConnectionConfiguration.serverLocation+":"+ConnectionConfiguration.port+"/";
        URL url = new URL(address + urlString);
        System.out.println(url.toString());
        URLConnection connection = url.openConnection();
        if (!(connection instanceof HttpURLConnection)) {
            throw new IOException("Not an HTTP Connection");
        } else {
            try {
                HttpURLConnection httpURLConnection = (HttpURLConnection) connection;
                httpURLConnection.setAllowUserInteraction(false);
                httpURLConnection.setInstanceFollowRedirects(true);
                httpURLConnection.setRequestMethod(method);
                httpURLConnection.setRequestProperty("Content-Type", "application/json");
                httpURLConnection.setDoInput(true);
                httpURLConnection.setDoOutput(true);
                httpURLConnection.connect();
                if (method.equals("POST")) {
                    httpURLConnection.getOutputStream().write(params.getBytes());
                }
                int response = httpURLConnection.getResponseCode();
                System.out.println("RESPONSE " + response);
                if (response == HttpURLConnection.HTTP_OK) {
                    inputStream = httpURLConnection.getInputStream();
                }
                return inputStream;
            } catch (Exception e) {
                throw new IOException("Error Connecting");
            }
        }
    }

    public static String getResponse(InputStream inputStream) throws IOException {
        String responseString = "";
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"), 8);
            StringBuilder stringBuilder = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }
            inputStream.close();
            responseString = stringBuilder.toString();
            return responseString;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return responseString;
    }

    public static String postJsonFromUrl(String address, String data) throws IOException {

        address="http://"+ConnectionConfiguration.serverLocation+":"+ConnectionConfiguration.port+"/"+address;

        URL url = new URL(address);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();

        con.setDoOutput(true);
        con.setDoInput(true);

        con.setRequestProperty("Content-Type","application/json");
        con.setRequestProperty("Accept","application/json");
        con.setRequestMethod("POST");

        OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
        //wr.write();

        return "";
    }


    public static String readJsonFromUrl(String url) throws IOException  {
        url="http://"+ConnectionConfiguration.serverLocation+":"+ConnectionConfiguration.port+"/"+url;
        System.out.println(url);
        InputStream is = new URL(url).openStream();
        try {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText = readAll(rd);
            return jsonText;
        } finally {
            is.close();
        }
    }
}
