package app.Data.DAL;

import app.Data.DataContainer.Dokter;
import app.Data.DataContainer.Pasien;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;

public class DokterDAL {

    public ObservableList<String> loadDokter() throws IOException {
        try {
            String resp = BaseDal.readJsonFromUrl("get/dokter");

            ObservableList<String> listDokter = FXCollections.observableArrayList();
            JSONObject response = (JSONObject) new JSONParser().parse(resp);

            if(response.get("success").toString().equals("true")){
                JSONArray dokters = (JSONArray) new JSONParser().parse(response.get("data").toString());
                for(int i =0; i < dokters.size(); i++){
                    Dokter p = new Dokter((JSONObject) dokters.get(i));
                    listDokter.add(p.getNama());
                }
                return listDokter;
            }else{
                throw new IOException("false");
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw new IOException(e);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}
