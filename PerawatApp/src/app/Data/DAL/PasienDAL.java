package app.Data.DAL;



import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import app.Data.DataContainer.Pasien;
import app.Data.DataContainer.Riwayat;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


public class PasienDAL {
    public ObservableList<Pasien> loadPasien() throws IOException {
        try {
            InputStream inputStream = BaseDal.OpenHttpConnection("get/pasien","GET", "");
            String result = BaseDal.getResponse(inputStream);

            ObservableList<Pasien> listPasien = FXCollections.observableArrayList();
            JSONObject response = (JSONObject) new JSONParser().parse(result);

            if(response.get("success").toString().equals("true")){
                JSONArray pasiens = (JSONArray) new JSONParser().parse(response.get("data").toString());
                for(int i =0; i < pasiens.size(); i++){
                    Pasien p = new Pasien((JSONObject) pasiens.get(i));
                    listPasien.add(p);
                }
                return listPasien;
            }else{
                throw new IOException("falses");
            }
        } catch (Exception e) {
                throw new IOException(e);
        }
    }
    public boolean pendaftaranPasien(String data){
        System.out.println("Here");
        try{
            InputStream inputStream = BaseDal.OpenHttpConnection("add/pasien", "POST", data);
            String result = BaseDal.getResponse(inputStream);
            System.out.println(result);

            JSONObject response = (JSONObject) new JSONParser().parse(result);
            if(response.get("success").toString().equals("true")) {
                return true;
            }else{
                return false;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return true;
    }


    public boolean checkIn(String data){
        System.out.println("Here");
        try{
            InputStream inputStream = BaseDal.OpenHttpConnection("add/riwayat", "POST", data);
            String result = BaseDal.getResponse(inputStream);
            System.out.println(result);

            JSONObject response = (JSONObject) new JSONParser().parse(result);
            if(response.get("success").toString().equals("true")) {
                return true;
            }else{
                return false;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return true;
    }

    public boolean editPasien(String data){
        System.out.println("Here");
        try{
            InputStream inputStream = BaseDal.OpenHttpConnection("edit/pasien", "POST", data);
            String result = BaseDal.getResponse(inputStream);
            System.out.println(result);

            JSONObject response = (JSONObject) new JSONParser().parse(result);
            if(response.get("success").toString().equals("true")) {
                return true;
            }else{
                return false;
            }
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    public ObservableList<Riwayat> loadRiwayat(int i) throws IOException{

        try {
            InputStream inputStream = BaseDal.OpenHttpConnection("get/riwayat/"+i,"GET", "");
            String result = BaseDal.getResponse(inputStream);

            ObservableList<Riwayat> listRiwayat = FXCollections.observableArrayList();
            JSONObject response = (JSONObject) new JSONParser().parse(result);

            if(response.get("success").toString().equals("true")){
                JSONArray pasiens = (JSONArray) new JSONParser().parse(response.get("data").toString());
                for(int x =0; x < pasiens.size(); x++){
                    Riwayat p = new Riwayat((JSONObject) pasiens.get(x));
                    listRiwayat.add(p);
                }
                return listRiwayat;
            }else{
                throw new IOException("false");
            }
        } catch (Exception e) {
            System.out.println("hai");
            e.printStackTrace();
            throw new IOException("false");
        }
    }
}
