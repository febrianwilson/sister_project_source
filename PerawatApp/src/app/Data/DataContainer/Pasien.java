package app.Data.DataContainer;

import org.json.simple.JSONObject;

public class Pasien {
    private Integer id_pasien;
    private String ktp;
    private String nama;
    private String jenisKelamin;
    private String tanggalLahir;
    private String golDarah;
    private String noTelp;
    private String alamat;
    private Integer dokter;

    public Pasien(Integer id_pasien, String ktp, String nama, String jenisKelamin, String tanggalLahir, String golDarah, String noTelp, String alamat) {
        this.id_pasien = id_pasien;
        this.ktp = ktp;
        this.nama = nama;
        this.jenisKelamin = jenisKelamin;
        this.tanggalLahir = tanggalLahir;
        this.golDarah = golDarah;
        this.noTelp = noTelp;
        this.alamat = alamat;
    }
    public Pasien(){

    }

    public Pasien(JSONObject jsonObject){
        this.id_pasien = Integer.parseInt(jsonObject.get("id_pasien").toString());
        this.ktp = jsonObject.get("ktp").toString();
        this.nama = jsonObject.get("nama").toString();
        this.jenisKelamin = jsonObject.get("jenisKelamin").toString();
        this.tanggalLahir = jsonObject.get("tanggalLahir").toString();
        this.golDarah = jsonObject.get("golDarah").toString();
        this.noTelp = jsonObject.get("noTelp").toString();
        this.alamat = jsonObject.get("alamat").toString();

    }

    public Integer getId() {
        return id_pasien;
    }

    public void setId(Integer id) {
        this.id_pasien = id;
    }

    public String getKtp() {
        return ktp;
    }

    public void setKtp(String ktp) {
        this.ktp = ktp;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getJenisKelamin() {
        return jenisKelamin;
    }

    public void setJenisKelamin(String jenisKelamin) {
        this.jenisKelamin = jenisKelamin;
    }

    public String getTanggalLahir() {
        return tanggalLahir;
    }

    public void setTanggalLahir(String tanggalLahir) {
        this.tanggalLahir = tanggalLahir;
    }

    public String getGolDarah() {
        return golDarah;
    }

    public void setGolDarah(String golDarah) {
        this.golDarah = golDarah;
    }

    public String getNoTelp() {
        return noTelp;
    }

    public void setNoTelp(String noTelp) {
        this.noTelp = noTelp;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }


    public Integer getDokter() {
        return dokter;
    }

    public void setDokter(Integer dokter) {
        this.dokter = dokter;
    }
}
