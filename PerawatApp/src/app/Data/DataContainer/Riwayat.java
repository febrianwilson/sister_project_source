package app.Data.DataContainer;

import org.json.simple.JSONObject;

public class Riwayat {


    protected String alamat;
    protected int checked;
    protected String diagnosa;
    protected String golDarah;
    protected int id_pasien;
    protected int id_dokter;
    protected int id_transaksi;
    protected String jenisKelamin;
    protected String keluhan;
    protected String ktp;
    protected String namaDokter;
    protected String namaPasien;
    protected String noTelp;
    protected String no_handphone;
    protected String spesialis;
    protected String tanggal;
    protected String tanggalLahir;


    public Riwayat(JSONObject jsonObject){
        this.alamat = jsonObject.get("alamat").toString();
        this.checked = Integer.parseInt(jsonObject.get("checked").toString());
        this.diagnosa = jsonObject.get("diagnosa").toString();
        this.golDarah = jsonObject.get("golDarah").toString();
        this.id_pasien = Integer.parseInt(jsonObject.get("id_pasien").toString());
        this.id_dokter = Integer.parseInt(jsonObject.get("id_dokter").toString());
        this.id_transaksi = Integer.parseInt(jsonObject.get("id_transaksi").toString());
        this.jenisKelamin = jsonObject.get("jenisKelamin").toString();
        this.keluhan = jsonObject.get("keluhan").toString();
        this.ktp = jsonObject.get("ktp").toString();
        this.namaDokter = jsonObject.get("namaDokter").toString();
        this.namaPasien = jsonObject.get("namaPasien").toString();
        this.noTelp = jsonObject.get("noTelp").toString();
        this.no_handphone = jsonObject.get("no_handphone").toString();
        this.spesialis = jsonObject.get("spesialis").toString();
        this.tanggal = jsonObject.get("tanggal").toString();
        this.tanggalLahir = jsonObject.get("tanggalLahir").toString();

    }

    public Riwayat(){

    }



    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public int getChecked() {
        return checked;
    }

    public void setChecked(int checked) {
        this.checked = checked;
    }

    public String getDiagnosa() {
        return diagnosa;
    }

    public void setDiagnosa(String diagnosa) {
        this.diagnosa = diagnosa;
    }

    public String getGolDarah() {
        return golDarah;
    }

    public void setGolDarah(String golDarah) {
        this.golDarah = golDarah;
    }

    public int getId_pasien() {
        return id_pasien;
    }

    public void setId_pasien(int id_pasien) {
        this.id_pasien = id_pasien;
    }

    public int getId_dokter() {
        return id_dokter;
    }

    public void setId_dokter(int id_dokter) {
        this.id_dokter = id_dokter;
    }

    public int getId_transaksi() {
        return id_transaksi;
    }

    public void setId_transaksi(int id_transaksi) {
        this.id_transaksi = id_transaksi;
    }

    public String getJenisKelamin() {
        return jenisKelamin;
    }

    public void setJenisKelamin(String jenisKelamin) {
        this.jenisKelamin = jenisKelamin;
    }

    public String getKeluhan() {
        return keluhan;
    }

    public void setKeluhan(String keluhan) {
        this.keluhan = keluhan;
    }

    public String getKtp() {
        return ktp;
    }

    public void setKtp(String ktp) {
        this.ktp = ktp;
    }

    public String getNamaDokter() {
        return namaDokter;
    }

    public void setNamaDokter(String namaDokter) {
        this.namaDokter = namaDokter;
    }

    public String getNamaPasien() {
        return namaPasien;
    }

    public void setNamaPasien(String namaPasien) {
        this.namaPasien = namaPasien;
    }

    public String getNoTelp() {
        return noTelp;
    }

    public void setNoTelp(String noTelp) {
        this.noTelp = noTelp;
    }

    public String getNo_handphone() {
        return no_handphone;
    }

    public void setNo_handphone(String no_handphone) {
        this.no_handphone = no_handphone;
    }

    public String getSpesialis() {
        return spesialis;
    }

    public void setSpesialis(String spesialis) {
        this.spesialis = spesialis;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getTanggalLahir() {
        return tanggalLahir;
    }

    public void setTanggalLahir(String tanggalLahir) {
        this.tanggalLahir = tanggalLahir;
    }




}
