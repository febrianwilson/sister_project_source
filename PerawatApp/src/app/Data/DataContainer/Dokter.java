package app.Data.DataContainer;


import org.json.simple.JSONObject;

public class Dokter {

    private Integer id_dokter;
    private String nama;
    private String spesialis;
    private String no_handphone;

    public Dokter(Integer id_dokter, String nama, String spesialis, String no_handphone) {
        this.id_dokter = id_dokter;
        this.nama = nama;
        this.spesialis = spesialis;
        this.no_handphone = no_handphone;
    }

    public Dokter(JSONObject jsonObject){
        this.id_dokter = Integer.parseInt(jsonObject.get("id_dokter").toString());
        this.nama = jsonObject.get("nama").toString();
        this.no_handphone = jsonObject.get("no_handphone").toString();
        this.spesialis = jsonObject.get("spesialis").toString();
    }

    public Integer getId() {
        return id_dokter;
    }

    public void setId(Integer id) {
        this.id_dokter = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getSpesialis() {
        return spesialis;
    }

    public void setSpesialis(String spesialis) {
        this.spesialis = spesialis;
    }

    public String getNo_handphone() {
        return no_handphone;
    }

    public void setNo_handphone(String no_handphone) {
        this.no_handphone = no_handphone;
    }

}
