package sister.Controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import sister.Data.Config;

public class SetupController extends BaseController {
    @FXML TextField txtIP, txtID;

    @FXML
    public void handleBtnConnect() {
        String conf = txtIP.getText().toString();
        String id = txtID.getText().toString();
        Config.serverLocation = conf.contains(":") ? conf.split(":")[0] : conf;
        Config.port = conf.contains(":") ? Integer.parseInt(conf.split(":")[1]) : 5000;

        if (Config.checkConnection()) {
            Config.idDokter = Integer.parseInt(id);
            showAlert(Alert.AlertType.INFORMATION, "Information", null, "Connected");
            Stage stage = (Stage) txtIP.getScene().getWindow();
            stage.hide();
            FXMLLoader loader = newForm("mainmenu.fxml", "Daftar Pasien", 400, 600, true);
            stage.setOnCloseRequest(event -> {
                ((MainController)loader.getController()).refreshRiwayat.stop();
            });
            this.stage.showAndWait();
            stage.show();
        } else {
            showAlert(Alert.AlertType.INFORMATION, "Information", null, "Failed to connect");
        }
    }
}
