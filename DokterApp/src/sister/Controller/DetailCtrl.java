package sister.Controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;
import sister.Data.DataContainer.Dokter;
import sister.Data.DataContainer.Pasien;

/**
 * Created by Eileen on 10/30/2017.
 */
public class DetailCtrl {
    private Pasien currentPasien;
    private Dokter currentDokter;

    @FXML
    Label lblNama, lblKtp, lblTgl, lblGolDar, lblNoTelp, lblAlamat;

    public void setCurrentData(Pasien pasien, Dokter dokter) {
        this.currentPasien = pasien;
        this.currentDokter = dokter;
        lblNama.setText(this.currentPasien.getNama());
        lblKtp.setText(this.currentPasien.getKtp());
        lblTgl.setText(this.currentPasien.getTanggalLahir());
        lblGolDar.setText(this.currentPasien.getGolDarah());
        lblNoTelp.setText(this.currentPasien.getNoTelp());
        lblAlamat.setText(this.currentPasien.getAlamat());
    }

    @FXML
    public void handleBtnKembali(){
        try{
            FXMLLoader loader = new FXMLLoader(getClass().getResource("UI/diagnosa.fxml"));
            Parent entryForm = loader.load();
            Stage entryStage = new Stage();
            entryStage.setTitle("Diagnosa");
            entryStage.setScene(new Scene(entryForm));
            entryStage.setResizable(false);
            entryStage.show();
            entryStage.requestFocus();
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
