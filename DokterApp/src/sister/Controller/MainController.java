package sister.Controller;

import com.jfoenix.controls.JFXButton;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Duration;
import sister.Data.Config;
import sister.Data.DAL.RiwayatDAL;
import sister.Data.DataContainer.Riwayat;

import java.io.IOException;

public class MainController extends BaseController{

    @FXML
    private TableView<Riwayat> tblRiwayat;
    @FXML
    private TableColumn colID, colNama, colKtp;
    @FXML
    private JFXButton btnDiagnosa;

    public Timeline refreshRiwayat = null;
    private ObservableList<Riwayat> riwayats;
    private boolean dbStatus = true;

    public void initialize(){
        colID.setCellValueFactory(new PropertyValueFactory<Riwayat,Integer>("idTransaksi"));
        colNama.setCellValueFactory(new PropertyValueFactory<Riwayat,String>("pasienNama"));
        colKtp.setCellValueFactory(new PropertyValueFactory<Riwayat,String>("pasienKTP"));

        tblRiwayat.setItems(riwayats);
        new Thread(() -> {
            ObservableList<Riwayat> tmp;
            try {
                tmp = RiwayatDAL.loadRiwayatByDokter(Config.idDokter);
            } catch (IOException e) {
                if (e.getMessage().equals("DB Down") && dbStatus) {
                    showFailDB();
                    dbStatus = false;
                } else if (e.getMessage().equals("DB Down") && !dbStatus){
                    System.out.println("DB still down");
                } else {
                    showFail();
                    checkConnection();
                }
                return;
            }
            if (!dbStatus) {
                showOK();
            }
            dbStatus = true;
            riwayats = tmp;
            tblRiwayat.setItems(riwayats);
        }).start();

        refreshRiwayat = new Timeline(new KeyFrame(Duration.seconds(Config.interval), event -> {
            ObservableList<Riwayat> tmp;
            try {
                tmp = RiwayatDAL.loadRiwayatByDokter(Config.idDokter);
            } catch (IOException e) {
                if (e.getMessage().equals("DB Down") && dbStatus) {
                    showFailDB();
                    dbStatus = false;
                } else if (e.getMessage().equals("DB Down") && !dbStatus) {
                    System.out.println("DB Still down");
                } else {
                    refreshRiwayat.stop();
                    btnDiagnosa.setDisable(true);
                    showFail();
                    checkConnection();
                }
                return;
            }
            if (!dbStatus) {
                showOK();
            }
            dbStatus = true;
            riwayats = tmp;
            tblRiwayat.setItems(riwayats);
        }));
        refreshRiwayat.setCycleCount(Timeline.INDEFINITE);
        refreshRiwayat.play();
    }

    @FXML
    public void handleBtnDiagnosa(){
        try{
            Riwayat currentRiwayat = tblRiwayat.getSelectionModel().getSelectedItem();
            if (currentRiwayat == null) return;
            FXMLLoader loader = newForm("diagnosa.fxml", "Diagnosa", 400, 600, false);
            DiagnosaCtrl diagnosaCtrl = loader.getController();
            diagnosaCtrl.setCurrentRiwayat(currentRiwayat);
            diagnosaCtrl.setParentController(this);
            stage.showAndWait();
        } catch (Exception e) { e.printStackTrace(); }
    }

    public void deleteRiwayat(Riwayat riwayat) {
        try {
            riwayats.remove(riwayat);
        } catch (Exception e) { e.printStackTrace(); }
    }

    public void showOK() {
        showAlert(Alert.AlertType.INFORMATION, "Information", null, "Server is online...", false);
    }

    public void showFail() {
        showAlert(Alert.AlertType.WARNING, "Information", null, "Server is offline\nTrying to connect on background...", false);
    }

    public void showFailDB() {
        showAlert(Alert.AlertType.WARNING, "Information", null, "Server is online\nBut unable to connect to database, please wait...", false);
    }

    public void checkConnection() {
        connectionChecker = new Timeline(new KeyFrame(Duration.seconds(Config.interval), event -> {
            System.out.println("Trying to connect...");
            if (Config.checkConnection()) {
                showOK();
                connectionChecker.stop();
                refreshRiwayat.play();
                btnDiagnosa.setDisable(false);
            } else {
                System.out.println("Failed to connect");
            }
        }));
        connectionChecker.setCycleCount(Animation.INDEFINITE);
        connectionChecker.play();
    }

}
