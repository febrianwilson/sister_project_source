package sister.Controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import sister.Data.DAL.RiwayatDAL;
import sister.Data.DataContainer.Riwayat;

/**
 * Created by Eileen on 10/30/2017.
 */
public class RiwayatCtrl {
    @FXML
    public TableView<Riwayat> tblRiwayat;
    @FXML
    public TableColumn colID, colTglKonsultasi, colDokter, colKeluhan, colDiagnosa;

    public ObservableList<Riwayat> riwayats = FXCollections.observableArrayList();

    public void initialize() {
        colID.setCellValueFactory(new PropertyValueFactory<Riwayat, Integer>("idTransaksi"));
        colTglKonsultasi.setCellValueFactory(new PropertyValueFactory<Riwayat,String>("tanggal"));
        colDokter.setCellValueFactory(new PropertyValueFactory<Riwayat,String>("namaDokter"));
        colKeluhan.setCellValueFactory(new PropertyValueFactory<Riwayat,String>("keluhan"));
        colDiagnosa.setCellValueFactory(new PropertyValueFactory<Riwayat,String>("diagnosa"));
        tblRiwayat.setItems(riwayats);
    }

    public void setIdPasien(int idPasien) {
        new Thread(() -> {
            ObservableList<Riwayat> listRiwayat = RiwayatDAL.loadRiwayat(idPasien);
            riwayats.clear();
            riwayats.setAll(listRiwayat);
        }).start();
    }
}
