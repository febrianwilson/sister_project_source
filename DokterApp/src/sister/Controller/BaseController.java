package sister.Controller;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;
import javafx.util.Duration;
import sister.Data.Config;

public class BaseController {

    public Timeline connectionChecker = null;
    public Stage stage = null;

    protected FXMLLoader newForm(String resource, String title, double height, double width, boolean showAndWait){
        resource = "../UI/" + resource;
        System.out.println(resource);

        FXMLLoader loader = null;
        try{

            loader = new FXMLLoader(getClass().getResource(resource));
            Parent parent = loader.load();
            Stage stage = new Stage();
            stage.setTitle(title);
            stage.setScene(new Scene(parent, width, height));
            stage.setResizable(false);
            this.stage = stage;
        }catch (Exception ex){
            ex.printStackTrace();
        }

        return loader;
    }

    //Usage example: showAlert(Alert.AlertType.WARNING, "Warning Information", null, "Username / Password Incorrect");
    public static void showAlert(Alert.AlertType alertType, String title, String header, String content) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(content);
        alert.showAndWait();
    }

    public static void showAlert(Alert.AlertType alertType, String title, String header, String content, boolean isWait) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(content);
        alert.show();
    }

}

