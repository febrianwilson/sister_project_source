package sister.Controller;

import com.jfoenix.controls.JFXButton;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import sister.Data.DAL.RiwayatDAL;
import sister.Data.DataContainer.Riwayat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import static java.util.Calendar.YEAR;

/**
 * Created by Eileen on 10/30/2017.
 */
public class DiagnosaCtrl extends BaseController {
    private Riwayat currentRiwayat;
    private MainController parentController;

    @FXML
    public Label lblNama, lblTgl,lblUmur,lblGolDar;
    @FXML
    public TextArea txtKeluhan, txtDiagnosa;
    @FXML
    public JFXButton btnSimpan;


    public void setCurrentRiwayat(Riwayat currentRiwayat) {
        this.currentRiwayat = currentRiwayat;
        lblNama.setText(this.currentRiwayat.getPasienNama());
        lblTgl.setText(this.currentRiwayat.getPasien().getTanggalLahir());
        lblGolDar.setText(this.currentRiwayat.getPasien().getGolDarah());
        txtKeluhan.setText(this.currentRiwayat.getKeluhan());

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        try {
            int diff = 0;
            Calendar now = Calendar.getInstance();
            Calendar born = Calendar.getInstance();
            born.setTime(sdf.parse(this.currentRiwayat.getPasien().getTanggalLahir()));

            System.out.println(born.get(YEAR));
            while(born.before(now)) {
                born.add(YEAR, 1);
                if (born.before(now)) diff++;
            }
            lblUmur.setText(Integer.toString(diff));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void handleBtnDetail(){
        try {
            FXMLLoader loader = newForm("detail.fxml", "Detail Pasien", 400, 600, false);
            DetailCtrl detailCtrl = loader.getController();
            detailCtrl.setCurrentData(this.currentRiwayat.getPasien(), this.currentRiwayat.getDokter());
            stage.showAndWait();
        } catch (Exception e) { e.printStackTrace(); }
    }

    @FXML
    public void handleBtnRiwayat(){
        try {
            FXMLLoader loader = newForm("riwayat.fxml", "Riwayat Pasien", 400,600, false);
            RiwayatCtrl riwayatCtrl = loader.getController();
            riwayatCtrl.setIdPasien(currentRiwayat.getPasienId());
            stage.show();
        } catch (Exception e) { e.printStackTrace(); }
    }

    @FXML
    public void handleBtnSimpan(){
        String keluhan = txtKeluhan.getText();
        String diagnosa  = txtDiagnosa.getText();
        currentRiwayat.setKeluhan(keluhan);
        currentRiwayat.setDiagnosa(diagnosa);

        //Stage s = (Stage) btnSimpan.getScene().getWindow();
        //s.close();
        new Thread(() -> {
            if (RiwayatDAL.updateRiwayat(currentRiwayat)) {
                Platform.runLater(() -> {
                    showAlert(Alert.AlertType.INFORMATION, "Information", null, "Diagnosa tersimpan");
                    parentController.deleteRiwayat(currentRiwayat);
                });
            } else {
                Platform.runLater(() -> showAlert(Alert.AlertType.WARNING, "Warning", null, "Gagal menyimpan"));
            }
        }).start();

    }

    public void setParentController(MainController mainController) {
        this.parentController = mainController;
    }
}
