package sister.Data.DataContainer;

/**
 * Created by Eileen on 10/30/2017.
 */
public class Dokter {
    private int id_dokter;
    private String nama;
    private String spesialis;
    private String no_handphone;

    public Dokter(int id_dokter, String nama, String spesialis, String no_handphone){
        this.id_dokter = id_dokter;
        this.nama = nama;
        this.spesialis = spesialis;
        this.no_handphone = no_handphone;
    }
    public int getIdDokter(){
        return this.id_dokter;
    }
    public void setIdDokter(int id_dokter){
        this.id_dokter = id_dokter;
    }
    public String getNama(){
        return this.nama;
    }
    public void setNama(String nama){
        this.nama = nama;
    }
    public String getSpesialis(){
        return this.spesialis;
    }
    public void setSpesialis(String spesialis){
        this.spesialis = spesialis;
    }
    public String getNoHandphone(){
        return this.no_handphone;
    }
    public void setNoHandphone(String no_handphone){
        this.no_handphone = no_handphone;
    }
}
