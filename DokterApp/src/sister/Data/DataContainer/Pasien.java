package sister.Data.DataContainer;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * Created by Eileen on 10/30/2017.
 */
public class Pasien {
    private int id_pasien;
    private String nama;
    private String ktp;
    private String golDarah;
    private String tanggalLahir;
    private String noTelp;
    private String jenisKelamin;
    private String alamat;

    public Pasien(int id_pasien, String nama, String ktp, String golDarah, String tanggalLahir, String noTelp, String jenisKelamin, String alamat){
        this.id_pasien = id_pasien;
        this.nama = nama;
        this.ktp = ktp;
        this.golDarah = golDarah;
        this.tanggalLahir = tanggalLahir;
        this.noTelp = noTelp;
        this.jenisKelamin = jenisKelamin;
        this.alamat = alamat;
    }
    public int getId_pasien(){
        return this.id_pasien;
    }
    public void setId_pasien(int id_pasien){
        this.id_pasien = id_pasien;
    }
    public String getNama(){
        return this.nama;
    }
    public void setNama(String nama){
        this.nama = nama;
    }
    public String getKtp(){
        return this.ktp;
    }
    public void setKtp(String ktp){
        this.ktp = ktp;
    }
    public String getGolDarah(){
        return this.golDarah;
    }
    public void setGolDarah(String golDarah){
        this.golDarah = golDarah;
    }
    public String getTanggalLahir(){
        return this.tanggalLahir;
    }
    public void setTanggalLahir(String tanggalLahir){
        this.tanggalLahir = tanggalLahir;
    }
    public String getNoTelp(){
        return this.noTelp;
    }
    public void setNoTelp(String noTelp){
        this.noTelp = noTelp;
    }
    public String getJenisKelamin(){
        return this.jenisKelamin;
    }
    public void setJenisKelamin(String jenisKelamin){
        this.jenisKelamin = jenisKelamin;
    }
    public String getAlamat(){
        return this.alamat;
    }
    public void setAlamat(String alamat){
        this.alamat = alamat;
    }
}
