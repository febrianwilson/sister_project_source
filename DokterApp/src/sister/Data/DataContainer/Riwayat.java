package sister.Data.DataContainer;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * Created by Eileen on 10/30/2017.
 */
public class Riwayat {

    private int id_transaksi;
    private String tanggal;
    private Pasien id_pasien;
    private Dokter id_dokter;
    private String keluhan;
    private String diagnosa;

    public Riwayat() {}

    public Riwayat(int id_transaksi, String tanggal, Pasien id_pasien, Dokter id_dokter, String keluhan, String diagnosa){
        this.id_transaksi = id_transaksi;
        this.tanggal = tanggal;
        this.id_pasien = id_pasien;
        this.id_dokter = id_dokter;
        this.keluhan = keluhan;
        this.diagnosa = diagnosa;
    }
    /*
        Untuk ambil data pasien di window 1
     */
    public int getPasienId() {
        return this.id_pasien.getId_pasien();
    }
    public String getPasienNama() {
        return this.id_pasien.getNama();
    }
    public String getPasienKTP() {
        return this.id_pasien.getKtp();
    }
    public void setPasienId(int id) {
        this.id_pasien.setId_pasien(id);
    }
    public void setPasienNama(String nama) {
        this.id_pasien.setNama(nama);
    }
    public void setPasienKTP(String ktp) {
        this.id_pasien.setKtp(ktp);
    }

    public String getNamaDokter() {
        return this.id_dokter.getNama();
    }

    public int getIdTransaksi(){
        return this.id_transaksi;
    }
    public void setIdTransaksi(int id_transaksi){
        this.id_transaksi = id_transaksi;
    }
    public String getTanggal(){
        return this.tanggal;
    }
    public void setTanggal(String tanggal){
        this.tanggal = tanggal;
    }
    public Pasien getPasien(){
        return this.id_pasien;
    }
    public void setPasien(Pasien id_pasien){
        this.id_pasien = id_pasien;
    }
    public Dokter getDokter(){
        return this.id_dokter;
    }
    public void setDokter(Dokter id_dokter){
        this.id_dokter = id_dokter;
    }
    public String getKeluhan(){
        return this.keluhan;
    }
    public void setKeluhan(String keluhan){
        this.keluhan = keluhan;
    }
    public String getDiagnosa(){
        return this.diagnosa;
    }
    public void setDiagnosa(String diagnosa){
        this.diagnosa = diagnosa;
    }
}
