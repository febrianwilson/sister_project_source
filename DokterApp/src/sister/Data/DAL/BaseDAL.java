package sister.Data.DAL;

import sister.Data.Config;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

public class BaseDAL {

    public static InputStream openHttpConnection(String urlString, String method, String params) throws IOException {
        InputStream inputStream = null;
        String address ="http://"+ Config.serverLocation+":"+Config.port+"/";
        URL url = new URL(address + urlString);
        System.out.println(url.toString());
        URLConnection connection = url.openConnection();
        if (!(connection instanceof HttpURLConnection)) {
            throw new IOException("Not an HTTP Connection");
        } else {
            try {
                HttpURLConnection httpURLConnection = (HttpURLConnection) connection;
                httpURLConnection.setAllowUserInteraction(false);
                httpURLConnection.setInstanceFollowRedirects(true);
                httpURLConnection.setRequestMethod(method);
                httpURLConnection.setRequestProperty("Content-Type", "application/json");
                httpURLConnection.setDoInput(true);
                httpURLConnection.setDoOutput(true);
                httpURLConnection.connect();
                if (method.equals("POST")) {
                    httpURLConnection.getOutputStream().write(params.getBytes());
                }
                int response = httpURLConnection.getResponseCode();
                System.out.println("RESPONSE " + response);
                if (response == HttpURLConnection.HTTP_OK) {
                    inputStream = httpURLConnection.getInputStream();
                }
                return inputStream;
            } catch (Exception e) {
                throw new IOException("Error Connecting");
            }
        }
    }

    public static String getResponse(InputStream inputStream) throws IOException {
        String responseString = "";
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"), 8);
            StringBuilder stringBuilder = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }
            inputStream.close();
            responseString = stringBuilder.toString();
            return responseString;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return responseString;
    }

}
