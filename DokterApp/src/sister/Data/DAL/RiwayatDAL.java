package sister.Data.DAL;

import com.google.gson.Gson;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import sister.Data.DataContainer.Dokter;
import sister.Data.DataContainer.Pasien;
import sister.Data.DataContainer.Riwayat;

import java.io.IOException;
import java.io.InputStream;

public class RiwayatDAL extends BaseDAL {

    public static ObservableList<Riwayat> loadRiwayat() throws IOException{
        InputStream inputStream = null;
        try {
            inputStream = openHttpConnection("get/riwayat", "GET", "");
            String result = getResponse(inputStream);

            ObservableList<Riwayat> listRiwayat = FXCollections.observableArrayList();
            JSONObject response = (JSONObject) new JSONParser().parse(result);
            if (response.get("success").equals("true")) {
                JSONArray riwayats = (JSONArray) new JSONParser().parse(response.get("data").toString());
                for (int i = 0; i < riwayats.size(); i++) {
                    JSONObject obj = (JSONObject)riwayats.get(i);
                    Pasien pasien = new Pasien(
                            Integer.parseInt(obj.get("id_pasien").toString()),
                            obj.get("namaPasien").toString(),
                            obj.get("ktp").toString(),
                            obj.get("golDarah").toString(),
                            obj.get("tanggalLahir").toString(),
                            obj.get("noTelp").toString(),
                            obj.get("jenisKelamin").toString(),
                            obj.get("alamat").toString()
                    );
                    Dokter dokter = new Dokter(
                            Integer.parseInt(obj.get("id_dokter").toString()),
                            obj.get("namaDokter").toString(),
                            obj.get("spesialis").toString(),
                            obj.get("no_handphone").toString()
                    );
                    Riwayat riwayat = new Riwayat(
                            Integer.parseInt(obj.get("id_transaksi").toString()),
                            obj.get("tanggal").toString(),
                            pasien,
                            dokter,
                            obj.get("keluhan").toString(),
                            obj.get("diagnosa").toString()
                    );
                    listRiwayat.add(riwayat);
                }
                return listRiwayat;
            }
        } catch (IOException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ObservableList<Riwayat> loadRiwayat(int idPasien) {
        try {
            InputStream inputStream = openHttpConnection("get/riwayat/"+idPasien, "GET", "");
            if (inputStream == null) return null;
            String result = getResponse(inputStream);

            ObservableList<Riwayat> listRiwayat = FXCollections.observableArrayList();
            JSONObject response = (JSONObject) new JSONParser().parse(result);
            if (response.get("success").equals("true")) {
                JSONArray riwayats = (JSONArray) new JSONParser().parse(response.get("data").toString());
                for (int i = 0; i < riwayats.size(); i++) {
                    JSONObject obj = (JSONObject)riwayats.get(i);
                    Pasien pasien = new Pasien(
                            Integer.parseInt(obj.get("id_pasien").toString()),
                            obj.get("namaPasien").toString(),
                            obj.get("ktp").toString(),
                            obj.get("golDarah").toString(),
                            obj.get("tanggalLahir").toString(),
                            obj.get("noTelp").toString(),
                            obj.get("jenisKelamin").toString(),
                            obj.get("alamat").toString()
                    );
                    Dokter dokter = new Dokter(
                            Integer.parseInt(obj.get("id_dokter").toString()),
                            obj.get("namaDokter").toString(),
                            obj.get("spesialis").toString(),
                            obj.get("no_handphone").toString()
                    );
                    Riwayat riwayat = new Riwayat(
                            Integer.parseInt(obj.get("id_transaksi").toString()),
                            obj.get("tanggal").toString(),
                            pasien,
                            dokter,
                            obj.get("keluhan").toString(),
                            obj.get("diagnosa").toString()
                    );
                    listRiwayat.add(riwayat);
                }
                return listRiwayat;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ObservableList<Riwayat> loadRiwayatByDokter(int idDokter) throws IOException {
        try {
            InputStream inputStream = openHttpConnection("get/riwayat/dokter/" + idDokter, "GET", "");
            if (inputStream == null) return null;
            String result = getResponse(inputStream);

            ObservableList<Riwayat> listRiwayat = FXCollections.observableArrayList();
            JSONObject response = (JSONObject) new JSONParser().parse(result);
            if (response.get("success").equals("true")) {
                JSONArray riwayats = (JSONArray) new JSONParser().parse(response.get("data").toString());
                for (int i = 0; i < riwayats.size(); i++) {
                    JSONObject obj = (JSONObject) riwayats.get(i);
                    Pasien pasien = new Pasien(
                            Integer.parseInt(obj.get("id_pasien").toString()),
                            obj.get("namaPasien").toString(),
                            obj.get("ktp").toString(),
                            obj.get("golDarah").toString(),
                            obj.get("tanggalLahir").toString(),
                            obj.get("noTelp").toString(),
                            obj.get("jenisKelamin").toString(),
                            obj.get("alamat").toString()
                    );
                    Dokter dokter = new Dokter(
                            Integer.parseInt(obj.get("id_dokter").toString()),
                            obj.get("namaDokter").toString(),
                            obj.get("spesialis").toString(),
                            obj.get("no_handphone").toString()
                    );
                    Riwayat riwayat = new Riwayat(
                            Integer.parseInt(obj.get("id_transaksi").toString()),
                            obj.get("tanggal").toString(),
                            pasien,
                            dokter,
                            obj.get("keluhan").toString(),
                            obj.get("diagnosa").toString()
                    );
                    listRiwayat.add(riwayat);
                }
                return listRiwayat;
            } else {
                throw new IOException("DB Down");
            }
        } catch (IOException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean updateRiwayat(Riwayat riwayat) {
        String data = new Gson().toJson(riwayat);
        try {
            InputStream inputStream = openHttpConnection("edit/riwayat", "POST", data);
            if (inputStream == null) return false;
            String result = getResponse(inputStream);

            JSONObject response = (JSONObject) new JSONParser().parse(result);
            if (response.get("success").equals("true")) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) { e.printStackTrace(); }
        return false;
    }
}
