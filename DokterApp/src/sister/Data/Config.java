package sister.Data;

import java.io.IOException;
import java.net.Socket;

public class Config {
    public static String serverLocation = "127.0.0.1";
    public static Integer port = 5000;
    public static int idDokter;

    private static final String username = "root";
    private static final String password = "";
    public static int interval = 10;


    public static boolean checkConnection(){
        try (Socket s = new Socket(serverLocation, port)) {
            return true;
        } catch (IOException ex) {
        /* ignore */
        }
        return false;
    }
}
